"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigFactory = void 0;
const class_transformer_1 = require("class-transformer");
class ConfigFactory {
    constructor(configSource) {
        this.configSource = configSource;
    }
    async create(configConstructor) {
        const config = new configConstructor;
        (0, class_transformer_1.plainToClassFromExist)(config, this.configSource.getConfig(config.getName(), config.getDefaults()));
        await config.validate();
        return config;
    }
}
exports.ConfigFactory = ConfigFactory;
//# sourceMappingURL=ConfigFactory.js.map